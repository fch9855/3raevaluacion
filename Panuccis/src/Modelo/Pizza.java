/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Logica.Factura;
import java.util.ArrayList;

/**
 *
 * @author Fernando Changoluisa
 */
public class Pizza {
    private ArrayList<Ingrediente> ingredientes;
    private double precio;
    private Factura factura; 
    
    
    public Pizza( ArrayList<Ingrediente> ingredientes, double precio){
        
        this.ingredientes = ingredientes;
        this.precio = precio;
        
    }
    
}
