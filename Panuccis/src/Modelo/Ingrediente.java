/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Fernando Changoluisa
 */
public class Ingrediente {
    private String imagen;
    private String nombre;
    private double precio;
    
    public Ingrediente(String nombre, String imagen, double precio){
        this.imagen= imagen;
        this.nombre = nombre;
        this.precio=precio;
    }
    
    
    public String toString(){
        return "precio: "+precio+", imagen:"+imagen;
    }

    public String getImagen() {
        return imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public double getPrecio() {
        return precio;
    }
    
    
    
    

    
}

