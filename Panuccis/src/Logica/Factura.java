/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author Fernando Changoluisa
 */
public class Factura {
    private String cedula;
    private String nombre;
    private String numero;
    private String direccion;
    
   
    
    public Factura(String cedula, String nombre, String numero,String direccion){
        this.cedula = cedula;
        this.nombre = nombre;
        this.numero = numero;
        this.direccion=direccion;
    }

    @Override
    public String toString() {
        return "Factura{" + "cedula=" + cedula + ", nombre=" + nombre + ", numero=" + numero + ", direccion=" + direccion + '}';
    }
    
    
}   
