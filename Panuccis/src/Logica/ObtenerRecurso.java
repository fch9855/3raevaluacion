/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import Interfaz.CONSTANTES;
import java.io.FileNotFoundException;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Border;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author Fernando Changoluisa
 */
public class ObtenerRecurso {
    
    public static StackPane abrirImagen (String nombre, double x, double y){
        try{
            ImageView img = new ImageView(new Image (CONSTANTES.imagen+nombre,x,y,false,false));
            StackPane imagen = new StackPane(img);
            return imagen;
            
        }catch(IllegalArgumentException e){
            //System.err.println(e.getMessage());
            Rectangle r = new Rectangle(x,y,Color.WHITE);
            Label l = new Label("Imagen Predeterminada");
            StackPane stack = new StackPane (r, l);
            stack.setStyle("-fx-border-color: black");
            
            return stack;
        }
    }
    
}
