/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import java.util.ArrayList;
import java.util.TreeMap;

import Interfaz.CONSTANTES;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


import Modelo.Ingrediente;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Fernando Changoluisa
 */
public class Lectura {
    private  TreeMap<String,Double> tamanos = new TreeMap<>();
    private  TreeMap<String,Ingrediente> ingredientes = new TreeMap<>();
    
    public Lectura(String tamanos, String ingredientes){
        cargarTamanos(tamanos);
        cargarIngredientes(ingredientes);
        
        
    }
    
    
    
     public TreeMap<String,Double> cargarTamanos(String nombreArchivo){
        ArrayList<String[]> t = new ArrayList<>();
        try(BufferedReader inputStream = new BufferedReader(new FileReader(CONSTANTES.ruta+nombreArchivo))){
            String l= null;
            while((l=inputStream.readLine())!= null){
                t.add(l.split(","));
             }
            
            
        }catch(FileNotFoundException e){
            System.out.println(e.getMessage());
            
        }catch(IOException e){
            System.out.println(e.getMessage());
            
        }
        
       
        for(String[] l : t){
            String s = l[1];
            Double d = Double.parseDouble(s);
            
            tamanos.putIfAbsent(l[0], d);
        }
        
        return tamanos;
    }
    
     public TreeMap<String,Ingrediente> cargarIngredientes(String nombreArchivo){
        
        try(BufferedReader inputStream = new BufferedReader(new FileReader(CONSTANTES.ruta+nombreArchivo))){
            String l= null;
            while((l=inputStream.readLine())!= null){
                String k = l.split(",")[0];
                Double p = Double.parseDouble(l.split(",")[1]);
                String i = l.split(",")[2];
                
                Ingrediente I = new Ingrediente(k,i,p);
                
                ingredientes.putIfAbsent(k, I);
                        
                
            
             }
            
            
        }catch(FileNotFoundException e){
            System.out.println(e.getMessage()); 
            
        }catch(IOException e){
            System.out.println(e.getMessage());
            
        }
       
        return ingredientes;
    }
    
    
    

    public  TreeMap<String, Double> getTamanos() {
        return tamanos;
    }

    public  TreeMap<String,Ingrediente> getIngredientes() {
        return ingredientes;
    }
    
    public Set<String> getIngredientesK(){
        return this.ingredientes.keySet();
    }
        
        
    
    public static void main(String[] args){
        Lectura l = new Lectura("Pizza.txt","Ingredientes.txt");
        System.out.println(l.getIngredientesK());
    }
       
      
        
        
    }
    
  
   
    
    
    
    
    
    

