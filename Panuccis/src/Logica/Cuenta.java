/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;


import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.text.Font;

/**
 *
 * @author Fernando Changoluisa
 */
public class Cuenta {
    private static double precio;
    Label l;
    ArrayList<Double> c;
    
    
    public static void HacerCuenta(ArrayList<Double> c, Label l){
        precio = 0;
        
        
        
        for(Double d: c){
            precio = precio + d;
        }
        
        l.setText("Total: "+precio);
        l.setFont(new Font(20));
        l.setStyle("-fx-text-fill: white;"
            +"-fx-font-weight: bold");
        
    }
        
   
}
