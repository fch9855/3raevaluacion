package Interfaz;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Fernando Changoluisa
 */
public class CONSTANTES {
    public static int ancho = 1280;
    public static int alto = 720;
    public static int rx =200;
    static int ry = 50;
    public static String ruta= "src/Recursos/";
    
    public static String imagen = "/Recursos/";
    
    public static Paint color = Color.DARKGREY;
    
    public static String formato= "-fx-background-color:DARKGREY";
    public static String formatoBlanco= "-fx-background-color:WHITE";
    
    public static double tfx = 500;
    public static double tfy = 20;
    
    public static double tax = 750;
    public static double tay = 400;
    
    public static double imgx = 80;
    public static double imgy = 80;
}
