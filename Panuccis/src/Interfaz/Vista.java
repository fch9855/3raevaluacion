/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;


//import Modelo.ArmarPizza;

/**
 *
 * @author Fernando Changoluisa
 */
public class Vista extends Application{
    
    static Stage ventana = new Stage(); 
   
    ArmarPizza p = new ArmarPizza();
    Scene pizza = new Scene(p.getRoot());
    
    PantallaPrincipal inicio = new PantallaPrincipal();
    
    @Override
    public void start(Stage ps){
        ventana = ps;
        Scene nuevo = new Scene(inicio.getRoot());
        ps.setScene(nuevo);
        ps.show();
        ps.setTitle("Pizzeria Panucci's");
        
        inicio.getInicio().setOnMouseClicked(new EventHandler<MouseEvent>(){
            public void handle(MouseEvent e){
                ps.setScene(pizza);
                
            }
        });
        
        p.getA().setOnMouseClicked(new EventHandler<MouseEvent>(){
            public void handle(MouseEvent e){
                ps.setScene(pizza);
            }
        });
        
    }

    public Stage getVentana() {
        return ventana;
    }
    
    
    
    
    public static void main(String[] args){
        launch(args);
    }
}