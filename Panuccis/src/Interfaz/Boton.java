/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import Logica.Lectura;
import javafx.scene.layout.VBox;

import Logica.ObtenerRecurso;
import Modelo.Ingrediente;
import java.util.ArrayList;
import javafx.scene.layout.HBox;
import Logica.Cuenta;
/**
 *
 * @author Fernando Changoluisa
 */
public class Boton {
    private Rectangle r;
    private StackPane stack;
    private String nombre;
    
    private HBox Bingrediente;
    
    Lectura lectura = new Lectura("Pizza.txt","Ingredientes.txt");
    
    
    
    
    public Boton(String nombre){
        this.nombre = nombre;
        r = new Rectangle(CONSTANTES.rx,CONSTANTES.ry, CONSTANTES.color);
        Label l = new Label(nombre);
        
        l.setFont(new Font(20));
        l.setStyle("-fx-text-fill: white;"
                   +"-fx-font-weight: bold");
        
        stack = new StackPane(r,l);
        
        stack.setOnMouseEntered(new EventHandler<MouseEvent>(){
            public void handle(MouseEvent e){
                r.setFill(Color.FIREBRICK);
            }
        });
        
        stack.setOnMouseExited(new EventHandler<MouseEvent>(){
            public void handle(MouseEvent e){
                r.setFill(CONSTANTES.color);
            }
        });
        
        stack.setAlignment(Pos.CENTER);
    }
    
    public Boton(String nombre , VBox saldo,ArrayList<Double> precio,Label t){
        this.nombre = nombre;
        r = new Rectangle(CONSTANTES.rx,CONSTANTES.ry, CONSTANTES.color);
        Label l = new Label(nombre);
        
        l.setFont(new Font(20));
        l.setStyle("-fx-text-fill: white;"
                   +"-fx-font-weight: bold");
        
        stack = new StackPane(r,l);
        
        stack.setOnMouseEntered(new EventHandler<MouseEvent>(){
            public void handle(MouseEvent e){
                r.setFill(Color.FIREBRICK);
            }
        });
        
        stack.setOnMouseExited(new EventHandler<MouseEvent>(){
            public void handle(MouseEvent e){
                r.setFill(CONSTANTES.color);
            }
        });
        
        stack.setAlignment(Pos.CENTER);
        
        double f = saldo.getSpacing();
        
        if(f==0){
        stack.setOnMouseClicked(new EventHandler<MouseEvent>(){
            public void handle(MouseEvent e){
                
                String ta= nombre;
                
                double valor = lectura.getTamanos().get(nombre.toLowerCase());
                String svalor = valor+"";
                Label l = new Label("Tamaño: "+nombre.toLowerCase()+"--"+"$"+valor);
                l.setFont(new Font(20));
                l.setStyle("-fx-text-fill: white;"
                   +"-fx-font-weight: bold");
               
                saldo.getChildren().add(l);
                saldo.setSpacing(20);
                precio.add(valor);
                
                Cuenta.HacerCuenta(precio, t);
                
                
                
                stack.setOnMouseClicked(new EventHandler<MouseEvent>(){
                    public void handle(MouseEvent e){
                        saldo.getChildren().remove(l);
                        saldo.setSpacing(0);
                        precio.remove(valor);
                        Cuenta.HacerCuenta(precio, t);
                   }
                });
            }
            });
        }
        
    }
    
    public Boton(String nombre, String ruta,VBox saldo,ArrayList<Double> precio,Label t,ArrayList<Ingrediente>i){
        Bingrediente = new HBox();
        String r = CONSTANTES.imagen+ ruta;
        //System.out.println(r);
        StackPane img = ObtenerRecurso.abrirImagen(r, CONSTANTES.imgx, CONSTANTES.imgy);
        Label n = new Label(nombre.toUpperCase());
        n.setFont(new Font(20));
                n.setStyle("-fx-text-fill: black;"
                   +"-fx-font-weight: bold");

        
        Bingrediente.getChildren().addAll(img,n);
        
        //Estilo
        Bingrediente.setMinSize(200, 30);
        Bingrediente.setAlignment(Pos.CENTER);
        Bingrediente.setSpacing(15);
        
        //Accion
        Bingrediente.setOnMouseClicked(new EventHandler<MouseEvent>(){
            public void handle(MouseEvent e){
                
                Ingrediente ing = lectura.getIngredientes().get(nombre.toLowerCase());
                
                String ing_n= ing.getNombre();
                double ing_p = ing.getPrecio();
                
                String preciotos = ing_p+"";
                
                Label l = new Label("Ingredente: "+nombre.toLowerCase()+"--"+"$"+ing_p);
                l.setFont(new Font(20));
                l.setStyle("-fx-text-fill: white;"
                   +"-fx-font-weight: bold");
               
                saldo.getChildren().add(l);
                saldo.setSpacing(20);
                precio.add(ing_p);
                System.out.println(precio);
                Cuenta.HacerCuenta(precio, t);
                i.add(lectura.getIngredientes().get(ing_n));
                
            }
        });
        
        
        
        
        
        
        
        
    }
    
   

    public StackPane getStack() {
        return stack;
    }

    public String getNombre() {
        return nombre;
    }

    public HBox getBingrediente() {
        return Bingrediente;
    }
    
    
    
    
    
}
