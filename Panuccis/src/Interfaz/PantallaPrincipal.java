/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Fernando Changoluisa
 */
public class PantallaPrincipal {
    private Boton inicio;
    private Boton consulta;
    private BorderPane root;
    private VBox menu;
    
    public PantallaPrincipal(){
        root = new BorderPane();
        root.setCenter(opciones());
        root.setPrefSize(CONSTANTES.ancho, CONSTANTES.alto);
        root.maxHeight(CONSTANTES.ancho);
        root.maxWidth(CONSTANTES.alto);
        root.setStyle("-fx-background-image: url('/Recursos/fondo.png');"
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size: "+CONSTANTES.ancho+" "+CONSTANTES.alto+"; "
                + "-fx-background-position: center center;");
    }
        
    /**
     * Metodo que permite construir un menu principal 
     * con las opciones de un nuevo pedido o de 
     * consulta de datos.
     * @return menu
     */
    public VBox opciones(){
        inicio = new Boton("Nuevo Pedido");
        consulta = new Boton("Consulta de ventas");
        menu = new VBox();
        menu.getChildren().addAll(inicio.getStack(),consulta.getStack());
        menu.setSpacing(10);
        menu.setAlignment(Pos.CENTER);
        
        
        //Cambio de escena
        
        
        return menu;     
        
     }

    public BorderPane getRoot() {
        return root;
    }

    public StackPane getInicio() {
        return inicio.getStack();
    }

    public StackPane getConsulta() {
        return consulta.getStack();
    }

    public VBox getMenu() {
        return menu;
    }
    
    
    
    
    
 
    
    
    
}
