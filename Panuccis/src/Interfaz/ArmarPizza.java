/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz;


import Logica.Factura;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;


import Logica.Lectura;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;

import javafx.scene.text.Font;
import Logica.ObtenerRecurso;
import Modelo.Ingrediente;
import Modelo.Pizza;
import Modelo.Ventas;
import java.util.ArrayList;
import java.util.TreeMap;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 *
 * @author Fernando Changoluisa
 */
public  class ArmarPizza {//implements EventHandler<ActionEvent> {
    private BorderPane root;
    private VBox botonera;
    private HBox centro;
    private Boton t = new Boton("Tamaño");
    private Boton i = new Boton("Ingredientes");
    private Boton f = new Boton("Facturacion");
    private Boton a = new Boton("Atras");
    
    Lectura lectura = new Lectura("Pizza.txt","Ingredientes.txt");
    
    private VBox valores = new VBox();
    
     
    HBox tamanos;
    
    //Factura
    private TextField ci;
    private TextField nom;
    private TextField num;
    private TextArea d;
    
    private double costo;
    
    private BorderPane VistaSaldo;
    
    ArrayList<StackPane> Btmns = new ArrayList<>();
    Set<String> k = lectura.getIngredientesK();
    
    ArrayList<Double> precio=new ArrayList<>();
    
    Label total =  new Label();
    
    
    
    Factura factura;
    ArrayList<Ingrediente> ingredientes = new ArrayList<Ingrediente>();
    
    String tamano ;
    
    
    
    
    
    
    public ArmarPizza(){
        
        root = new BorderPane();
        root.setMinSize(CONSTANTES.ancho, CONSTANTES.alto);
        
        root.setTop(generarTop());
        root.setLeft(generarBotonera());
        root.setRight(generarDetalle());
        generarTamano();
        
        
        
        t.getStack().setOnMouseClicked(new EventHandler<MouseEvent>(){
            public void handle(MouseEvent e){
                root.setCenter(generarTamano());
            }
        });
        
        f.getStack().setOnMouseClicked(new EventHandler<MouseEvent>(){
            public void handle(MouseEvent e){
                root.setCenter(vistaFactura());
            }
        });
        
        i.getStack().setOnMouseClicked(new EventHandler<MouseEvent>(){
            public void handle(MouseEvent e){
                root.setCenter(vistaIngredientes());
            }
        });
        
        System.out.println(precio);
        
        
                
    }
            
    
    
    
    public VBox generarBotonera(){
        botonera = new VBox();
       
        botonera.getChildren().addAll(t.getStack(),i.getStack(),f.getStack(),a.getStack());
        
        botonera.setAlignment(Pos.BASELINE_LEFT);
        botonera.setLayoutX(10);
        botonera.setSpacing(12);
        
        botonera.setMinSize(CONSTANTES.rx, CONSTANTES.alto);
        
        botonera.setStyle(CONSTANTES.formato);
        
        
        
        return botonera;
        
        
        
    }
    
    public HBox generarTop(){
        Label l = new Label("ARMA TU PIZZA");
        l.setAlignment(Pos.CENTER);
        l.setFont(new Font(20));
        l.setStyle("-fx-text-fill: white;"
            +"-fx-font-weight: bold");
        
        HBox hb = new HBox(l);
        
        hb.setMinSize(CONSTANTES.ancho, 40);
        hb.setSpacing(0);
        hb.setStyle("-fx-background-color:CRIMSON");
        hb.setAlignment(Pos.CENTER);
        
        
        
        
        return  hb;
        
    
    }
    
    public VBox generarTamano(){
        VBox muestra = new VBox();
        tamanos = new HBox();
        
        muestra.setStyle(CONSTANTES.formatoBlanco);
        
        
        double d =tamanos.getSpacing();

        StackPane pg = new StackPane(ObtenerRecurso.abrirImagen("pizza.png",200 , 200));
        StackPane pm = new StackPane(ObtenerRecurso.abrirImagen("pizza.png",150 , 150));
        StackPane pp = new StackPane(ObtenerRecurso.abrirImagen("pizza.png",100 , 100));

        pg.setMinSize(200, 200);
        pm.setMinSize(200, 200);
        pp.setMinSize(200, 200);


        HBox pizzas = new HBox(pg,pm,pp);


        pizzas.setAlignment(Pos.CENTER);
        pizzas.setSpacing(15);


        
            muestra.getChildren().add(pizzas);
            Set<String> llaves = new TreeSet<>();
            llaves = lectura.getTamanos().keySet();
            
            for (String k : llaves){
                Boton t = new Boton(k.toUpperCase(Locale.ITALY), valores,precio,total);
                Btmns.add(t.getStack());
                tamanos.getChildren().addAll(t.getStack());
                
             }
           
            
            
            
            tamanos.setAlignment(Pos.BOTTOM_CENTER);

            
            tamanos.setSpacing(15);

            muestra.getChildren().add(tamanos);
            muestra.setAlignment(Pos.CENTER);
            muestra.setSpacing(80);
            muestra.setStyle(CONSTANTES.formatoBlanco);
        
                
            System.out.println(precio);
        
        return muestra;
        
    }
    
    public BorderPane generarDetalle(){
        VistaSaldo = new BorderPane();
        
                
        Label saldo = new Label(" Detalle Saldo:");
        
        
       
        
        
        saldo.setFont(new Font(20));
        saldo.setStyle("-fx-text-fill: white;"
            +"-fx-font-weight: bold");
        
        VistaSaldo.setTop(saldo);
        VistaSaldo.setBottom(total);
        VistaSaldo.setCenter(valores);
        
        
        
        VistaSaldo.setStyle("-fx-background-color:DARKGREY");
        VistaSaldo.setMinSize(CONSTANTES.rx+80, CONSTANTES.alto);
        VistaSaldo.setMaxSize(CONSTANTES.rx+80, CONSTANTES.alto);
        total.setAlignment(Pos.BOTTOM_CENTER);
        saldo.setAlignment(Pos.TOP_CENTER);
        
        
        
      return VistaSaldo;  
    }
    
    public VBox vistaFactura(){
        VBox centro = new VBox();
        HBox fac = new HBox();
        StackPane s = new StackPane();
        s.setMinSize(20, 50);
        
        
        StackPane lci = new StackPane(new Label("Cedula:"));
        StackPane lnom = new StackPane(new Label("Nombre:"));
        StackPane ltlf = new StackPane (new Label("Num. Telefono:"));
        StackPane ldir = new StackPane (new Label("Direccion:"));
        
        lci.setMinSize(50, 20);
        lnom.setMinSize(50, 20);
        ltlf.setMinSize(50, 20);
        ldir.setMinSize(50, 20);
        
        
        
        VBox etiqueta = new VBox(lci,lnom,ltlf,ldir);
        
        
        ci = new TextField();
        nom = new TextField();
        num = new TextField();
        d = new TextArea();
        
        ci.setMaxSize(CONSTANTES.tfx, CONSTANTES.tfy);
        nom.setMaxSize(CONSTANTES.tfx, CONSTANTES.tfy);
        num.setMaxSize(CONSTANTES.tfx, CONSTANTES.tfy);
        d.setMaxSize(CONSTANTES.tax, CONSTANTES.tay);
        
        VBox campo = new VBox(ci, nom,num,d);
        
        etiqueta.setSpacing(30);
        
        campo.setSpacing(20);
        
        
        fac.getChildren().addAll(etiqueta,campo);
        fac.setSpacing(20);
        fac.setAlignment(Pos.CENTER);
        
        Boton comprar = new Boton("Finalizar Compra");
        
        
        centro.getChildren().addAll(fac,comprar.getStack());
        
        
                
        comprar.getStack().setOnMouseClicked(new EventHandler<MouseEvent>(){
            public void handle(MouseEvent e){
            Factura factura = new Factura (ci.getText(),nom.getText(),num.getText(),d.getText());
            
            

            }
        });
         
                
        return centro;       
    }
    
    public VBox vistaIngredientes(){
        VBox lista_ingredientes = new VBox();
        
        
        //System.out.println(k);
        for(String s: k){
            
            
            String ruta = lectura.getIngredientes().get(s).getImagen();
          
            
            Boton bingrediente = new Boton(s,ruta,valores,precio,total,ingredientes);
           
            
            lista_ingredientes.getChildren().addAll(bingrediente.getBingrediente());
            
            
            
        }
        System.out.println(precio);
        
        lista_ingredientes.setAlignment(Pos.CENTER);
        lista_ingredientes.setSpacing(30);
        
        return lista_ingredientes;
    }
     
    
    public BorderPane getRoot() {
        return root;
    }

    public StackPane getA() {
        return a.getStack();
    }
   
    
    public void GenerarVentas(){
        Double preciofinal =Double.parseDouble(total.getText());
        Pizza p = new Pizza(ingredientes,preciofinal);
        
        Ventas ventas = new Ventas(p,factura);
    
    }
    
    
}


